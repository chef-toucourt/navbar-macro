<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Chef-Toucourt
 */
final class MacroController extends AbstractController
{
    /**
     * @Route(
     *     "/",
     *     name="app_macro_index",
     *     methods={"GET"}
     * )
     */
    public function index(): Response
    {
        return $this->render('macro/index.html.twig');
    }

    /**
     * @Route(
     *     "/another-route",
     *     name="app_macro_another_route",
     *     methods={"GET"}
     * )
     */
    public function anotherRoute(): Response
    {
        return $this->render('macro/another-template.html.twig');
    }
}
